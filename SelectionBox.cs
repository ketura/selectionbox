﻿///////////////////////////////////////////////
// The MIT License (MIT)
//
// Copyright (c) 2015 Christian 'ketura' McCarty
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
///////////////////////////////////////////////

/*
INSTRUCTIONS FOR USE:

Simply create an empty GameObject in Unity and add this script as a component. Then, on the LineRenderer,
change the Size variable (under the Positions section) to 8.

See http://i.imgur.com/lChakhW.png .

*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(LineRenderer))]
public class SelectionBox : MonoBehaviour
{
  public LineRenderer SelectionBoxGUI;
  public List<GameObject> Selection;

  private Vector2 SelectStart;
  private Vector2 SelectEnd;
  private bool CurrentlySelecting = false;

  //used to track whether the mouse buttons are being held down.
  private bool LeftDown = false;
  private bool RightDown = false;

  // Use this for initialization
  void Start()
  {
    SelectionBoxGUI = GetComponent<LineRenderer>();
    Selection = new List<GameObject>();
  }

  // Update is called once per frame
  void Update()
  {
    //Process the input.  In my setup, this section is actually part of an input controller object,
    // but it works here and is included for completion's sake.  If you were to incorporate any kind
    // of advanced commands beyond attack and move (such as targeted spells, patrols, etc), then
    // this will need to be aware of that so it doesn't change the selection when the player was trying
    // to issue advanced orders.

    //This will of course need to be customized to whatever you call the LMB input.  By default it's "Fire", I believe.
    if (Input.GetButton("Select"))
    {
      if (!LeftDown)
      {
        StartSelection(Input.mousePosition);
        LeftDown = true;
      }
      else
        MoveSelection(Input.mousePosition);
    }
    else
    {
      ReleaseSelection(Input.mousePosition);
      LeftDown = false;
    }

    //And same here for RMB.
    if (Input.GetButton("Action"))
    {
      if (!RightDown)
      {
        ActionButton(Input.mousePosition);
        RightDown = true;
      }
    }
    else
      RightDown = false;

    //This section actually controls the look of the selection box itself.
    if (CurrentlySelecting)
    {
      SelectionBoxGUI.enabled = true;

      //The line renderer is not a gui element, remember, it's a world object.  This means if the
      // camera has zoomed too far out, you will only be able to see it sporatically.  This keeps 
      // an ~1-2 pixel width, I've found. 
      float width = 0.01f * Camera.main.orthographicSize;
      SelectionBoxGUI.SetWidth(width, width);

      //this is arbitrary for 2D games.  For 3D, this probably needs to be linked to the camera's z.
      // Making the line renderer a child of the camera will not work, as the component's transform is
      // more or less irrelevant to the actual lines, since you will be defining their z component here in 
      // this function.
      float depth = -8f;

      //To actually be able to use the default linerenderer, you need twice as many points
      // as one might assume.  So to make a 4-point rectangle, we need 8 points, and the 
      // corners all need to be "twisted" to make the whole thing line up. Think of making
      // the whole thing out of a long twist-tie; you can't just bend the corners if you 
      // want it to line up straight.

      //Segments 1 and 5 (the end stroke of the vertical segments) vary their x value by a small amount
      // depending on whether the player drew the box right-to-left or left-to-right.
      float xoffset;
      if (SelectEnd.x > SelectStart.x)
        xoffset = -0.01f;
      else
        xoffset = 0.01f;

      //First segment
      SelectionBoxGUI.SetPosition(0, new Vector3(SelectStart.x, SelectStart.y, depth));
      SelectionBoxGUI.SetPosition(1, new Vector3(SelectEnd.x + xoffset, SelectStart.y, depth));

      //Second segment.  Vertex 3 varies in a similar manner to verticies 1 and 5, but based on
      // whether the player went up-to-down or down-to-up.
      SelectionBoxGUI.SetPosition(2, new Vector3(SelectEnd.x, SelectStart.y, depth));
      if (SelectEnd.y < SelectStart.y)
        SelectionBoxGUI.SetPosition(3, new Vector3(SelectEnd.x, SelectEnd.y + 0.01f, depth));
      else
        SelectionBoxGUI.SetPosition(3, new Vector3(SelectEnd.x, SelectEnd.y - 0.01f, depth));

      //Third segment.
      SelectionBoxGUI.SetPosition(4, new Vector3(SelectEnd.x, SelectEnd.y, depth));
      SelectionBoxGUI.SetPosition(5, new Vector3(SelectStart.x - xoffset, SelectEnd.y, depth));

      //Fourth segment.  Vertex 7 doesn't need vertex 3's adjustments, as it is the last point.
      SelectionBoxGUI.SetPosition(6, new Vector3(SelectStart.x, SelectEnd.y, depth));
      SelectionBoxGUI.SetPosition(7, new Vector3(SelectStart.x, SelectStart.y, depth));
    }
    else
      SelectionBoxGUI.enabled = false;
  }

  //The LMB has been pressed. The old selection is lost, and we start anew.
  public void StartSelection(Vector2 target)
  {
    target = Camera.main.ScreenToWorldPoint(target);
    CurrentlySelecting = true;
    SelectStart = target;
    SelectEnd = target;
    ClearSelection();
  }

  //The LMB is being held down and the mouse was dragged, so we need to start searching for new
  // objects to add to the selection.
  public void MoveSelection(Vector2 target)
  {
    target = Camera.main.ScreenToWorldPoint(target);
    if (CurrentlySelecting)
    {
      SelectEnd = target;
      SelectPlayersInBounds();
    }
  }

  //The LMB was released.  Eliminate the selection box from the screen, and select all the units within it.
  public void ReleaseSelection(Vector2 target)
  {
    target = Camera.main.ScreenToWorldPoint(target);
    if (CurrentlySelecting)
    {
      CurrentlySelecting = false;
      SelectEnd = target;
      SelectPlayersInBounds();
      /*
      foreach (GameObject go in Selection)
        go.GetComponent<SomeComponent>().Selected = true;
       */
    }
  }

  //The RMB was pressed.  In RTS-like controls, that means that the current selection recieves some sort of 
  // quick action command, usually move or attack.
  public void ActionButton(Vector2 point)
  {
    //right-clicked while in the middle of selection, however, should cancel the selection process.
    if (CurrentlySelecting)
    {
      CurrentlySelecting = false;
      ClearSelection();
    }
    else if (Selection.Count > 0)
    {
      //this code is commented out for the sake of the demo, but it's approximately what you'll need
      // to attack enemy units.  EnemyMask should be a public LayerMask variable on this component so 
      // you can set it in the inspector to whatever layer all your enemy units are on.

      /*
      point = Camera.main.ScreenToWorldPoint(point);
      RaycastHit2D hit = Physics2D.Raycast(point, Vector2.zero, Mathf.Infinity, EnemyMask);
      Creature target = null;

      if (hit.collider != null)
        target = hit.collider.gameObject.GetComponent<Creature>();
       */

      //If the attack raycast failed, then simply send your units the target point instead.

      /*
      foreach (GameObject go in Selection)
      {
        if (target != null)
          go.GetComponent<SomeComponent>().ChangeTarget(target);
        else
          go.GetComponent<SomeComponent>().ChangeTarget(point);
      }
      */
    }
  }

  private void SelectUnit(GameObject go)
  {
    Selection.Add(go);
    //let the unit know it's now selected (so it can show selection glow, etc).
    //go.GetComponent<SomeComponent>().Selected = true;
  }

  private void ClearSelection()
  {
    //announce to all the selection objects that they are no longer selected, so they can
    // disable selection ui, etc.
    /*
    foreach (GameObject go in Selection)
      go.GetComponent<SomeComponent>().Selected = false;
    */
    Selection.Clear();
  }


  //This is used to select a single unit, and would be used perhaps exclusively if you didn't
  // have a shnazzy box selector.  This is used by the SelectPlayersInBounds for edge cases.
  private void SelectSingleUnit(Vector2 target)
  {
    //All of this is commented out since it's implementation-specific.  This is basically what it
    // needs to work, however.

    /*
    RaycastHit2D hit = Physics2D.Raycast(target, Vector2.zero, Mathf.Infinity, AllyMask);

    if (hit.collider != null)
    {
      SomeComponent sc = hit.collider.GetComponentInParent<SomeComponent>();
      if (sc != null)
        SelectUnit(sc);
    }
    */
  }

  //The real meat of this component, selects units based on the fancy box select object you now have.
  private void SelectPlayersInBounds()
  {
    //The selection must first be cleared to make way for the new selection.  This means that units
    // will actually be selected/unselected once per frame so long as the player holds down the mouse.
    // Plan your SomeComponent.Select() functions accordingly.  This is done this way so that units
    // light up as you drag the mouse, without having to wait until the mouse is released.
    // You could probably rework this to use TempSelect and FinalizeSelect functions instead, if that
    // doesn't appeal to you.
    ClearSelection();

    //Most of this is still implemenation-specific, so it's commented out just so things don't break
    // the moment you import it into your scene.  

    /*

    //First we need a list of all selectable units.  A bit crude, but the alternative is to pop
    // the selection box full of raycasts and hope that we can get the casts dense enough to not
    // miss anything.

    List<Unit> Units = UnitController.Instance.ActiveUnits();


    */
    //Make a Bounds object equivelant to our selection box.  Since we don't know if the player has
    // gone upper-left-to-bottom-right or vice-versa, we need the min/max calls.
    Bounds selection = new UnityEngine.Bounds();
    selection.SetMinMax(Vector3.Min(SelectStart, SelectEnd), Vector3.Max(SelectStart, SelectEnd));
    
    //If the result is a point, then the player likely just made a click (or they've held the button
    // without moving the mouse), so we'll only try to select one unit.
    if (selection.extents == Vector3.zero)
      SelectSingleUnit(selection.center);

    /*
    //We now go through all possible selectable units and compare their position to the selection box.
    foreach (Unit u in Units)
    {
      // This is a static function in my project, defined as so:
      //  return (layerMask.value & (1 << obj.layer)) > 0;
      if (!UnitController.IsInLayerMask(p.gameObject, AllyMask))
        continue;

      //We need the sprite's size so as to get a semi-accurate selection.  Unit should probably keep track
      // of the actual SpriteRenderer.Bounds object (listed here as BoundingBox).

      if (u.BoundingBox.Intersects(selection))
        SelectUnit(u.gameObject);
    }
    */
  }
}
